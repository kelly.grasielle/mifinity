package com.minifinity.model;

import javax.persistence.*;

@Entity
public class Role {

    @Id
    private Integer id;
    @Column
    private String name;

    public Role(Integer id){
        this.id = id;
    }

    Role(){
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
