package com.minifinity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;


@EntityScan("com.minifinity.model")
@EnableJpaRepositories("com.minifinity.repository")
@SpringBootApplication(scanBasePackages = "com.minifinity")
@ComponentScan
public class AppMain {


    public static void main(String[] args) {
        SpringApplication.run(AppMain.class, args);
    }

}