package com.minifinity.service;


import com.minifinity.model.Creditcard;
import com.minifinity.model.User;
import com.minifinity.repository.CreditcardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CreditcardService {

    @Autowired
    CreditcardRepository repo;




    public List<Creditcard> getCreditcard(String number, String role, String username){

        if(role.equals("USER")){
            return repo.findByNumberContainingByCreator(number, username);
        }else{
            return repo.findByNumberContaining(number);
        }

    }

    public List<Creditcard> getCreditcard(String role, String username){

        if(role.equals("USER")){
            return repo.findByCreator(username);
        }else{
            return repo.findAll();
        }
    }

    public Optional<Creditcard> getCreditcardById(Integer id){

        return repo.findById(id);
    }

    public boolean checkDuplicidade(Creditcard creditcard){
        List list = new ArrayList();
        if(creditcard.getId() != null){
            list = repo.find(creditcard.getNumber(), creditcard.getId());
        }else{
            list = repo.findByNumber( creditcard.getNumber());
        }


        return !list.isEmpty();
    }

    public void update(Creditcard creditcard, String username){
        if(creditcard.getId() == null){
            creditcard.setCreator(new User(username));
        }

        repo.save(creditcard);
    }
}
