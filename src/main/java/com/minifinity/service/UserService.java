package com.minifinity.service;


import com.minifinity.model.Creditcard;
import com.minifinity.model.Role;
import com.minifinity.model.User;
import com.minifinity.repository.CreditcardRepository;
import com.minifinity.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository repo;

    public void save(User user){
        user.setRole(new Role(2));
        user.setPassword("{noop}" + user.getPassword());
        repo.save(user);
    }
}
