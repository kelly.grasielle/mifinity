package com.minifinity.repository;


import com.minifinity.model.Creditcard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;

public interface CreditcardRepository extends JpaRepository<Creditcard, Integer> {



    List<Creditcard> findByNumberContaining(String number);

    List<Creditcard> findByNumber(String number);

    @Query("Select x from Creditcard x where x.number = :number and x.id <> :id")
    List<Creditcard> find(@Param("number") String number, @Param("id") Integer id);

    @Query("Select x from Creditcard x where x.number like %:number% and x.creator.username = :creator")
    List<Creditcard> findByNumberContainingByCreator(@Param("number") String number, @Param("creator") String creator);

    @Query("Select x from Creditcard x where x.creator.username = :creator")
    List<Creditcard> findByCreator(@Param("creator") String creattor);

}
