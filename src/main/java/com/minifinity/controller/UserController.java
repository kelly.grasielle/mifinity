package com.minifinity.controller;

import com.minifinity.model.User;
import com.minifinity.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {

    @Autowired
    UserService service;


    @RequestMapping("/register")
    public String register(Model model){
        model.addAttribute("user", new User());

        return "register";
    }

    @RequestMapping("/login")
    public String login(){
        return "login";
    }

    @RequestMapping("/access-denied-")
    public String accessDenied(){
        return "accessDenied";
    }

    @RequestMapping(value ="/user", method = RequestMethod.POST )
    public String save(Model model, @ModelAttribute User user) {
        service.save(user);

        return "redirect:/login";
    }
}
