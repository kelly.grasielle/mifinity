package com.minifinity.controller;

import com.minifinity.model.Creditcard;
import com.minifinity.service.CreditcardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Controller
public class CreditcardController {

    @Autowired
    CreditcardService service;

    @RequestMapping("/creditcard/search")
    public String creditcardByNumber(Model model, @ModelAttribute Creditcard cardNumber){
        if(cardNumber.getNumber().equals("")){
            return "redirect:/creditcard";
        }
        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        String username = auth.getName();
        GrantedAuthority role = auth.getAuthorities().iterator().next();

        model.addAttribute("cardNumber", cardNumber);
        model.addAttribute("creditcardLst", service.getCreditcard(cardNumber.getNumber(), role.getAuthority(), username));
        return "creditcard";

    }

    @RequestMapping(value ="/creditcard", method = RequestMethod.GET )
    public String creditcard(Model model){
        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        String username = auth.getName();
        GrantedAuthority role = auth.getAuthorities().iterator().next();

        model.addAttribute("cardNumber", new Creditcard());
        model.addAttribute("creditcardLst", service.getCreditcard(role.getAuthority(), username));
        return "creditcard";
    }

    @RequestMapping("/creditcard/edit/{id}")
    public ModelAndView showEditModel(@PathVariable("id") Integer id, Model model) {

        Creditcard edit = service.getCreditcardById(id).get();
        model.addAttribute("creditcardEdit", edit);
        model.addAttribute("param.error", "");

        return new ModelAndView("creditcardEdit :: resultsList");
    }

    @RequestMapping("/creditcard/add")
    public ModelAndView showEditModel(Model model) {

        model.addAttribute("creditcardEdit", new Creditcard());

        return new ModelAndView("creditcardEdit :: resultsList");
    }

    @RequestMapping(value ="/creditcard", method = RequestMethod.POST )
    public String edit( Model model, @ModelAttribute Creditcard creditcard) throws Exception {

        if(service.checkDuplicidade(creditcard)){
            throw new Exception("The creditcard number: " + creditcard.getNumber() + " already exist.");
        }
        try {
            YearMonth ym = YearMonth.parse( creditcard.getExpiryDate() , DateTimeFormatter.ofPattern("MM/uuuu"));
        }catch (DateTimeParseException e){
            throw new Exception("Expiry date invalid: " + creditcard.getExpiryDate() + ". Please type a valid date MM/YYYY");
        }

        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        service.update(creditcard, auth.getName());

        return "redirect:/creditcard";
    }

    @RequestMapping(value ="/" )
    public String home() {

        return "redirect:/creditcard";
    }


}
